import React from 'react';
import {Col, Row} from 'reactstrap';

export function Step1Recommendation(props){
    if(props.currentStep !==1) return null;

    const MAX_BOXES = 11;
    const question = 'How likely are you to recommend Cole Haan to a friend or colleague';
    const arr = Array.from({length: MAX_BOXES}, (_, index) => index);
    const handleChange = (value) =>{
        props.handleChange(value);
    }

    // Use tabindex to make element seletable via css focus
    const boxes = arr.map((box, i) => 
        <Col sm={1}><span onClick={()=>{handleChange(i)}} 
            tabindex={i} className={'MSS-RecomendationBox'}>{i}</span>
        </Col>);

    return <div className={'MSS-PAD'}>
        <br />
        <Row>
            <p className={'MSS-Question'}>{question}?</p>
        </Row>
        <br />
        <Row>
            {boxes}
        </Row>
        <br />
    </div>
}