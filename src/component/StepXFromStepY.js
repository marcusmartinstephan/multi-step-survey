import React from 'react';


export function StepXFromStepY(props){
    return <div>
            <span className={'MSS-STEPFROMSTEP'}>
                Step {props.currentStep}/{props.maxSteps}
            </span>
        </div>
}