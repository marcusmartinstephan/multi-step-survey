import React from 'react';
import {Input, Row, Col} from 'reactstrap';

export function Step4EasyReach(props){
    if(props.currentStep !==4) return null;
    return <div className={'MSS-PAD'}>
        <br />
        How easy was it to accomplish that goal? <br /><br />
        <Row>
            <Col sm={1}></Col>
            <Col>
                <Input type="radio" name="easyreach" />Very Difficult
            </Col>
        </Row>
        <Row>
            <Col sm={1}></Col>
            <Col>
                <Input type="radio" name="easyreach" />Difficult
            </Col>
        </Row>
        <Row>
            <Col sm={1}></Col>
            <Col>
                <Input type="radio" name="easyreach" />Not Difficult nor easy
            </Col>
        </Row>
        <Row>
            <Col sm={1}></Col>
            <Col>
                <Input type="radio" name="easyreach" />Easy
            </Col>
        </Row>
        <Row>
            <Col sm={1}></Col>
            <Col>
                <Input type="radio" name="easyreach" />Very Easy
            </Col>
        </Row>
        <br />
    </div>
}