import React, {useState} from 'react';
import {MultiStepSurveyMainForm} from './MultiStepSurveyMainForm'

export function MultiStepSurvey(){
    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);

    return <div style={{width: '100%', minWidth: '100%'}}>
        <div className="MSS-Button-Container">
          <a onClick={toggle} href="# " className="MSS-Button MSS-ButtonOne">Start the Survey</a>
        </div>
        <MultiStepSurveyMainForm toggle={toggle} modal={modal}/>
    </div>
}