import React from 'react';
import {Input} from 'reactstrap';

export function Step3ChooseGoal(props){
    if(props.currentStep !==3) return null;
    return <div className={'MSS-PAD'}>
        <br />
        What was your goal for todays visit to ColeHaan.com?
        <br /><br />
        <Input type="select" name="goal" id="goal">
            <option>Browse new Arrivals</option>
            <option>Find a specific style</option>
        </Input>
        <br />
    </div>
}