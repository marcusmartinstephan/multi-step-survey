import React, {useState} from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Col, Row } from 'reactstrap';
import {Step1Recommendation} from './Step1Recommendation';
import {Step2ChooseCategory} from './Step2ChooseCategory';
import {Step3ChooseGoal} from './Step3ChooseGoal';
import {Step4EasyReach} from './Step4EasyReach';
import {StepXFromStepY} from './StepXFromStepY'

export function MultiStepSurveyMainForm(props){
  
    const [currentStep, setCurrentStep] = useState(1);
    const [recommendation, setRecommendation] = useState(0);
    const [category, setCategory] = useState();
    const [goal, setGoal] = useState();
    const [easyReach, setEasyReach] = useState();
    
    const MaxSteps = 4;
    
    // Trigger something on form submission
    const handleSubmit = (event) => {
      event.preventDefault()
    }

    const nextStep = () => {
      setCurrentStep(currentStep >= MaxSteps-1 ? MaxSteps: currentStep + 1);
    }
      
    const prevStep = () => {
      setCurrentStep(currentStep <= 1 ? 1: currentStep - 1);    
    }

    const previousButton = 
      (currentStep !==1)
      ?  <Button onClick={prevStep}  color="primary">Previous</Button>
      : null;
    
    const nextButton = 
      (currentStep < MaxSteps)
      ?  <Button onClick={nextStep} color="primary">Next</Button>
      :null;

    const submitButton = 
      (currentStep === MaxSteps)
      ?  <Button color="primary">Submit</Button>
      :null;

    return <Modal isOpen={props.modal} toggle={props.toggle}         
        autoFocus={true} centered={true} size={'lg'}>
        <ModalHeader>
          <Row>
            <Col sm={8}>
              Cole Haan Feedback
            </Col>
            <Col sm={4} className={'float-right'}>
              <StepXFromStepY currentStep={currentStep} maxSteps={MaxSteps} />
            </Col>
          </Row>
        </ModalHeader>
        <ModalBody>

        <form onSubmit={handleSubmit}>          

          <Step1Recommendation
            currentStep={currentStep} 
            handleChange={setRecommendation}
            recommendation={recommendation}
          />
          <Step2ChooseCategory
            currentStep={currentStep} 
            handleChange={setCategory}
            category={category}
          />
          <Step3ChooseGoal
            currentStep={currentStep} 
            handleChange={setGoal}
            goal={goal}
          />    
          <Step4EasyReach
            currentStep={currentStep} 
            handleChange={setEasyReach}
            goal={easyReach}
          />    
        </form>
        </ModalBody>
        <ModalFooter>
            <br /><br /><br />
            <Row style={{textAlign:'center'}}>
              <Col>{previousButton}</Col>
              <Col>{nextButton}</Col>
              <Button color="secondary" onClick={props.toggle}>Cancel</Button>
              <Col>{submitButton}</Col>            
            </Row>
        </ModalFooter>
      </Modal>
  }