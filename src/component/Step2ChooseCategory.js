import React from 'react';
import {Input} from 'reactstrap';

export function Step2ChooseCategory(props){
    if(props.currentStep !==2) return null;
    return <div className={'MSS-PAD'}>
        <br />
        Please choose a category to leave feedback about.
        <br /><br />
            <Input type="select" name="category" id="category">
            <option>Our Website</option>
            <option>Our Products</option>
            <option>Our Retail Locations</option>
            <option>Our Customer Service</option>
            </Input>
        <br />
    </div>
}